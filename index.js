
let express = require('express');
let app = express();
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()


const fs = require('fs')
const directory = "./api/files/"

function createFile(fileName, fileContent) {
    fs.writeFile(directory + fileName, fileContent, err => {
        if (err) throw err
    })
}
function getAllFileNamesFromDirectory() {
    const fileNames = [];
    const files = fs.readdirSync(directory);
    files.forEach(file => {
        fileNames.push(file);
    });
    return fileNames;
}
function getFileByName(fileName) {
    const stat = fs.statSync(directory + fileName)
    const content = fs.readFileSync(directory + fileName, 'utf8');
    let obj = {}
    obj.birthtime = stat.birthtime;
    obj.content = content;
    return obj;
}


app.get('/api/files', function (req, res) {
    let responseBody = {};
    try {
        responseBody.files = getAllFileNamesFromDirectory()
        responseBody.message = 'Success'
    }
    catch (err) {
        res.status(500)
        responseMessage.message = "Server error"
    }
    res.send(responseBody)
})

app.get('/api/files/:fileName', function (req, res) {
    let responseBody = {};
    try {
        let file = getFileByName(req.params.fileName)
        responseBody.message = 'Success';
        responseBody.filename = req.params.fileName;
        responseBody.content = file.content;
        responseBody.extension = (req.params.fileName).split(".")[1];
        responseBody.uploadedDate = file.birthtime;
    } catch (err) {
        if (err.code === 'ENOENT') {
            res.status(400);
            responseBody.message = `No file with '${req.params.fileName}' filename found`
        } else {
            res.status(500)
            responseMessage.message = "Server error"
        }
    }
    res.send(responseBody)
})

app.post('/api/files', jsonParser, function (req, res) {
    let responseMessage = {};

    if (!req.body.filename) {
        res.status(400)
        responseMessage.message = "Please specify 'filename' parameter"
    } else if (!req.body.content) {
        res.status(400)
        responseMessage.message = "Please specify 'content' parameter"
    } else {
        try {
            createFile(req.body.filename, req.body.content);
            responseMessage.message = "File created successfully"
        } catch (err) {
            res.status(500)
            responseMessage.message = "Server error"
        }
    }
    res.send(responseMessage)
})

app.listen(8080, function () {

})